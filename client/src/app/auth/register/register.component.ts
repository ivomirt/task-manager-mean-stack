import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private _snackBar: MatSnackBar
  ) {}

  registerForm: FormGroup;

  ngOnInit(): void {
    this.createForm();
  }
  createForm(): void {
    this.registerForm = this.fb.group({
      firstName: [''],
      lastName: [''],
      email: [
        '',
        Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ],
      password: ['', Validators.minLength(6)],
    });
  }

  onSubmit(): void {
    this.authService
      .registerUser(this.registerForm.value)
      .subscribe((data) => console.log(data));
    this.registerForm.reset();
    this._snackBar.open('Registration completed !', null, {
      duration: 3000,
    });
  }
}
