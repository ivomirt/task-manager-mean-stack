const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
require("dotenv/config");

const usersRoute = require("./routes/users");
const authRoute = require("./routes/auth");

app.use(express.json());

app.use(cors());

app.use("/api/users", usersRoute);

app.use("/api/auth", authRoute);

mongoose.connect(
  process.env.DB_CONNECTION,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log("connected to db");
  }
);

app.listen(3000, () => {
  console.log("server started");
});
