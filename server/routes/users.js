const express = require("express");

const router = express.Router();
const User = require("../models/User");
const verify = require("./privateRoutes");

//get all users
router.get("/", verify, async (req, res) => {
  try {
    const users = await User.find({}, { password: 0, date: 0 });

    res.json(users);
  } catch (err) {
    res.json({ message: err });
  }
});

//get user by ID
router.get("/:userId", verify, async (req, res) => {
  try {
    const user = await User.findById(req.params.userId, {
      password: 0,
      date: 0,
    });
    res.json(user);
  } catch (err) {
    res.json({ message: err });
  }
});

module.exports = router;
